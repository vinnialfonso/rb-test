Rails.application.routes.draw do

	post 'events', to: "events#index"
	get 'issues/:number/events', to: "events#issues"

end
