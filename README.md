**Dependencies**

* Ruby 2.6.3
* ngrok

**Installing ngrok**

Download ngrok in https://ngrok.com/download

Install ngrok

Run this command to save configuration authtoken ngrok:

./ngrok authtoken 1oWEw8Ot3DRFdnMHKQX63B21QHg_5rjF8rnPL9xrKJJSqeh1K

./ngrok http 3000

**Executing rails project**

Acesss project folder (rb-test)

Using rvm with ruby-2.6.3

`bundle install`

`rake db:create`

`rake db:migrate`

`rails s -p 3000 -b '0.0.0.0'`
