class Event < ActiveRecord::Base

	validates :action, presence: true

end