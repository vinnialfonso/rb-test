class EventsController < ApplicationController

  require 'json'

  def index
    begin
      req = JSON.parse(request.body.read)
      action = req.first[0]
      link = req.first[1]["links"]["html"]["href"] if action == "issue"
      event = Event.new({ action: action, link: link })
      if event.save
        render_json_success(:event, event)
      else
        render_json_errors(event.errors)
      end
    rescue StandardError => error
      render_json_errors(error.message)
    end
  end

  def issues
    begin
      issues = Event.where(action: "issue").limit(params[:number]).order(id: :desc)
      render_json_success(:issues, issues)
    rescue StandardError => error
      render_json_errors(error.message)
    end
  end

end