class ApplicationController < ActionController::Base
  protect_from_forgery with: :null_session

  def render_json_success key = nil, record = nil
    render_json = {}
    render_json[:success] = true
    render_json[key.to_sym] = record if key && record
    render json: render_json
  end

  def render_json_errors errors
    render_json = {}
    render_json[:success] = false
    render_json[:errors] = errors
    render json: render_json
  end
  
end
